﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GoldCounter : MonoBehaviour
{
    public Text goldText;
    public int gold;


    private void Start()
    {
        gold = 0;
    }
    void Update()
    {
        goldText.text = gold.ToString();
    }

    

}
