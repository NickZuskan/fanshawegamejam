﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PoliceBar : MonoBehaviour
{
    private Slider slider;
    private float targertProgress = 0f;
    public float FillSpeed = 0.5f;

    private void Awake()
    {
        slider = gameObject.GetComponent<Slider>();
    }
    
    void Start()
    {
        IncrementProgress(1f);
    }

    
    void Update()
    {
        if (slider.value < targertProgress)
            slider.value += FillSpeed * Time.deltaTime;

    }

    public void IncrementProgress (float newProgress)
    {
       targertProgress = slider.value + newProgress;
    }
}
