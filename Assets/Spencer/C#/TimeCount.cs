﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimeCount : MonoBehaviour
{
    [SerializeField] private float timeStart = 0f;
    public Text textBox;

     void Start()
     {
        textBox.text = timeStart.ToString();
     }

     void Update()
     {
        timeStart += Time.deltaTime;
        textBox.text = Mathf.Round (timeStart).ToString();
     }
}
