﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    
    public Transform spawnPosition;
    public GameObject player;
    
    


    private void Start()
    {

        var spawn = Instantiate(player, spawnPosition.position, spawnPosition.rotation);
        player = spawn;
    }
}




