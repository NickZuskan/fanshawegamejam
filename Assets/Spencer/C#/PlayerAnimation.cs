﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{


    static Animator anim;
    
    
    
    void Start()
    {
        anim = GetComponent<Animator>();
        
    }


    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            anim.SetTrigger("isAttacking");
        }

        if (Input.GetButtonDown("Fire2"))
        {
            anim.SetTrigger("isLooting");
        }

        if (Input.GetButton("Horizontal"))
        {
           anim.SetBool("isWalking", true);
        }
        else
        {
           anim.SetBool("isWalking", false);
        }

        if (Input.GetButton("Vertical"))
        {
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }

    }
}
